# Define required macros here


OBJS = ProccessUser.o proirterizer.o processKiller.o 
CFLAGS = -g -Wall -c linux/cCode/
CC = g++
LDFLAGS = -pthread -L/ProcessUser
PROG = procUser

$(PROG) : $(OBJS)
	$(CC) $(LDFLAGS) -o $(PROG) $(OBJS) 
proirterizer.o:
	$(CC) $(CFLAGS)proirterizer.cc
processKiller.o:
	$(CC) $(CFLAGS)processKiller.cc
ProccessUser.o:
	$(CC) $(CFLAGS)ProccessUser.cc

clean:
	-rm -f core $(PROG) $(OBJS)



#include<iostream>
#include<cstdlib>
#include<csignal>
#include "tools.h"
using namespace std;
int kill()
{   
    int ProcessId;
    bool killed = false; 
    system("ps -all");
    cout<<"Enter the Process ID to kill\n";
    cin>>ProcessId;
    if((kill(ProcessId,SIGKILL))){
        killed = true;
    }
    if(killed)
    {
        cout<<"List of processes after killing the process with PID"<<ProcessId<<"are"<<"\n";
        system("ps -all");
    }
    else    
    {   
        cout<<"Cant kill the process\n pls check if it exists or whether you have permission (try using sudo)" ;
    }   
    return 0;
}

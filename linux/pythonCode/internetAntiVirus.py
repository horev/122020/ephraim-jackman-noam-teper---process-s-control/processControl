import os
import csv


def data_to_csv(file_name , csv_file_name): #file - .txt with data
#the function check every line in the .txt file and then enters it to .csv file 
#file_name - .txt file , csv_file_name - .csv file , we use the func several times
	file = open('linux/Files/' + file_name + '.txt' , "r")
	with open( 'linux/Files/' + csv_file_name + '.csv' , mode="wb") as csv_file:
		csv_writer = csv.writer(csv_file, delimiter=',', quotechar='"',quoting=csv.QUOTE_NONE)
		if csv_file_name == 'list_ports':# we need once to egnore the first line in the file = list_ports
			counter = 0
		else: #the condition meant to resolve one time problem with the first file we turn txt to csv
			counter = 1
		for line in file:
			if "Active U" in line: #to use only active internet connections
				break
			elif counter != 0: #we need the data in .csv file to be without the first line of the .txt file
				line1=' '.join(line.split())
				output_list = line1.strip().split(' ')
				csv_writer.writerow(output_list)
			else:
				counter += 1
	file.close()

def find_suspicius_port():
	reader = csv.DictReader(open("linux/Files/list_ports.csv")) #making dict from .csv file
	port_counter = 0
	for row in reader:
		str_cut = row['Local']
		port_num = str_cut[str_cut.rfind(':')+1:len(str_cut)] #we only need the port number
		#print port_num - for future flag
		if int(port_num) > 6660 and int(port_num) < 7000: #ports bing used by tcp and udp 6660-7000
			print "suspectes port: " + port_num
			if find_and_kill(port_num): #if proc wasn't root
				port_counter += 1
			else:
				port_counter +=0
	if port_counter == 0: #means that there is no compremised ports
		print "your computer is secured"
	else: #there might be legitimate libraries
		print "your computer was compremized , pleas inspect  suspisious_proc_related_file.txt to check for deep inspection"


def find_and_kill(suspicius_port):
	os.system("lsof -RPni :" + suspicius_port + "> linux/Files/tmpFile.txt")
	data_to_csv('tmpFile' , 'procs_using_port')
	reader = csv.DictReader(open("linux/Files/procs_using_port.csv"))
	for row in reader:
		if row['USER'] == "root": #if the proc is running as root - we kill to be sure but not deleting files
			print "the proc using the port " + row['PID']
			os.system("lsof -Pnp " + int(row['PID']) + '> suspisious_proc_related_file.txt')
			os.system("kill " + str(int(row['PID'])))
			return True #returns true if the proc was malicious
		else:
			os.system("kill " + str(int(row['PID'])))
			return False #returns false if the proc was legit	


def main():
	os.system('netstat -an > linux/Files/tmpFile.txt')
	data_to_csv('tmpFile' , 'list_ports')
	find_suspicius_port()



	#file.close()
if __name__== "__main__":
	main()

import os
import time
from multiprocessing import Process
import csv


def create_new_data(): 
    #in the start creating new data
    print("creating new data pls wait a while...")
    run_john()
    data_to_percentage()
    
    
def run_john():
    #running john the ripper and putting it in a csv file 
    
    p1 = Process(target = terminaline, args=("john --test >> linux/Files/withProir.csv",))
    p1.start()
    time.sleep(1)
    pidJ = os.popen("pidof john").read()
    print("proir check: pls wait a moment.....")
    p2 = Process(target = terminaline, args=("sudo renice -n -19 -p " + str(pidJ),))
    p2.start()
    p3 = Process(target = terminaline, args=("john --test >> linux/Files/withoutProir.csv",))
    p3.start()

    p1.join()
    print("data with proir created successfully")
    p3.join()
    print("data without proir created successfully")
    
def terminaline(line):
    #writes whats given in the terminal
    os.system(str(line))



def update():
    #parallel to new data, those the same just first gets rid of the old data
    os.system("rm linux/Files/*.csv")
    run_john()
    data_to_percentage()

def data_to_percentage():
    #taking the data and turning it into useful data, using python package csv.
    #Note: no need to install csv
    avges = [] #list of the output
    for filename in ['withProir', 'withoutProir']: #going through both files
        avges.append(get_avg_cyph(filename)) 
    percentage = avges[0]/avges[1] * 100 - 100 #turning numbers to percent
    print ('proired by ' + str(int(percentage)) + '%') #printing percentage
    return percentage

def get_avg_cyph(filename):
    #turn data into one avg
    
    with open('linux/Files/' + filename + '.csv') as csv_file: #opening the csv file
        csv_reader = csv.reader(csv_file, delimiter=',') #reading csv file (delimeter = what you want to put between each word in the string default: ',' csvfile = csv file name)
        WP_Avg_Cyph = calculate_avg_cyphers(csv_reader) #turning the data into a number
        
        print("avg cyphes " + filename + ': ' + str(int(WP_Avg_Cyph))) #printing data
    return WP_Avg_Cyph


def calculate_avg_cyphers(csv_reader):
    #calculate the avg of a certain row
    
    count = 0 
    sum = 0
    for row in csv_reader: #going through each row
        accessible_list = " ".join(row).split() #technique to put the row(which is a string with ',' (delimeter) between each word) into a list.
        for i in range(len(accessible_list)): #looking for the numbers that always are followed by "c/s"
            if accessible_list[i] == 'c/s': 
                list_number = list(accessible_list[i-1]) #getting the number (i-1 = the word before)
                if list_number[-1] == 'K': #if there is a K = kilo(1000) multiply by 1000 note: the reason didnt do int(number) because of the K's 
                    list_number.remove('K') #removing the k
                    str_number = "".join(list_number) #turn to string
                    sum+=float(str_number + '000') #add the number to sum
                    count+= 1
                else:   
                    sum+=float(accessible_list[i-1]) #add number to sum
                count+= 1
    return sum/count #return avg





def main():
    try:
        with open('linux/Files/withProir.csv') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
        update()
    except:
        create_new_data()
    
if __name__== "__main__":
    main()


